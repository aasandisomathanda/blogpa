$(function () {
    var APPLICATION_ID = "AAD41445-D43E-6ECA-FF73-AEFD88C91E00", 
        SECRET_KEY = "4CFFBA9C-8542-D630-FF44-DF7CEC73D300",
        VERSION = "v1";
   
    
    Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION);
     
    var postsCollection = Backendless.Persistence.of(Posts).find();

    console.log(postsCollection);
    
    var wrapper = {
        posts: postsCollection.data    
    };
    
    Handlebars.registerHelper('format', function(time) {
        return moment(time).format("dddd, MMMM Do YYYY");
    });
    
    var blogScript = $("#blogs-template").html();
    var blogTemplate = Handlebars.compile(blogScript);
    var blogHTML = blogTemplate(wrapper);
    
    $('.main-container').html(blogHTML);
}); 

function Posts(args){
    args = args || {};
    this.title = args.title || "";
    this.content = args.content || "";
    this.authorEmail = args.authorEmail || "";
}


